#!/bin/bash
CPU_AVERAGE()
{
    top -b -n2 -p 1 | fgrep "Cpu(s)" | tail -1 | awk -F'id,' -v prefix="$prefix" '{ split($1, vs, ","); v=vs[length(vs)]; sub("%", "", v); printf "%s%.1f\n", prefix, 100 - v }'
}
RAM_AVERAGE()
{
    free -m | awk 'NR==2{printf "%sMB\n%sMB\n(%.2f)\n", $3,$2,$3*100/$2 }' | jq -sR '[sub("\n$";"") | splits("\n") | sub("^ +";"") | [splits(" +")]] | .[0] as $header | .[1:] | [.[] | [. as $x | range($header | length) | {"key": $header[.], "value":$x[.]}] | from_entries]'
}
UP_TIME()
{
    uptime | awk '{print $3,$4}' | cut -f1 -d,
}
OS_VERSION()
{
    lsb_release -a | grep 'Description'|awk '{print $2,$3}'
}
DISK_USAGES()
{
    df | sed -e "s/Mounted on/Mounted/g" | jq -sR '[sub("\n$";"") | splits("\n") | sub("^ +";"") | [splits(" +")]] | .[0] as $header | .[1:] | [.[] | [. as $x | range($header | length) | {"key": $header[.], "value":$x[.]}] | from_entries]'
}
HISTORY_COMMAND()
{
    history 20
}
CONTAINER_IN_DOCKER()
{
    docker ps
}
LAST_USER_ACCESS()
{
    last | head -n 10
}
RUNNING_PROCESS()
{
    ps aux | jq -sR '[sub("\n$";"") | splits("\n") | sub("^ +";"") | [splits(" +")]] | .[0] as $header | .[1:] | [.[] | [. as $x | range($header | length) | {"key": $header[.], "value":$x[.]}] | from_entries]'
}
LAST_BOOT()
{
    last reboot | head -1
}
Send_MainServer()
{
    curl --request POST \
  --url http://localhost:3000/mainserver \
  --header 'Content-Type: application/json' \
  --header 'enctype: multipart/form-data' \
  --form 'files=@temp/RAM.json' \
  --form 'files=@temp/RUNNING_PROCESS.json' \
  --form 'files=@temp/DISK.json'
}
#Function
# CPU=$(CPU_AVERAGE)
RAM=$(RAM_AVERAGE)
# UPTIME=$(UP_TIME)
# OS=$(OS_VERSION)
DISK=$(DISK_USAGES)
# HISTORY=$(HISTORY_COMMAND)
# DOCKER=$(CONTAINER_IN_DOCKER)
# USERACCESS=$(LAST_USER_ACCESS)
RUNNINGPROCESS=$(RUNNING_PROCESS)
# LASTBOOT=$(LAST_BOOT)
#File
# TEMPFILE_CPU="temp/CPU.txt"
TEMPFILE_RAM="temp/RAM.json"
# TEMPFILE_UPTIME="temp/UPTIME.txt"
# TEMPFILE_OS="temp/OS.txt"
TEMPFILE_DISK="temp/DISK.json"
# TEMPFILE_HISTORY="temp/HISTORY.txt"
# TEMPFILE_DOCKER="temp/DOCKER.txt"
# TEMPFILE_USERACCESS="temp/USERACCESS.txt"
TEMPFILE_RUNNINGPROCESS="temp/RUNNINGPROCESS.json"
# TEMPFILE_LASTBOOT="temp/LASTBOOT.txt"
#CONMAND
# echo $CPU > $TEMPFILE_CPU
echo $RAM > $TEMPFILE_RAM
# echo $UPTIME > $TEMPFILE_UPTIME
# echo $OS > $TEMPFILE_OS
echo $DISK > $TEMPFILE_DISK
# echo $HISTORY > $TEMPFILE_HISTORY
# echo $DOCKER > $TEMPFILE_DOCKER
# echo $USERACCESS > $TEMPFILE_USERACCESS
echo $RUNNINGPROCESS > $TEMPFILE_RUNNINGPROCESS
# echo $LASTBOOT > $TEMPFILE_LASTBOOT


sendResult=$(Send_MainServer)