﻿Requirement 

Disk space 3.75 MB

Ubuntu

	- apt-get install lsb-core jq lynis

CentOS

	- sudo yum install redhat-lsb-core epel-release jq lynis weasyprint -y
	
Service

	- ถ้าเครื่อง client ไม่มี service ใดๆ ให้ทำการ "off" service นั้นในไฟล์ cloud-monitor-clientconfig.conf
	- ใส่ CLIENT_KEY ในไฟล์ cloud-monitor-clientconfig.conf

Create user read-only for mysql
	- เข้า mysql โดยใช้ user และรหัสผ่าน ของ Client
	- mysql -u root -p     (Enterแล้วใส่Password)
	- หลังจากเข้ามาให้ สร้าง user ที่ใช้อ่านข้อมูลได้อย่างเดียว ไม่สามารถแก้ไขข้อมูลใน Database ได้
	- โดยรันคำสั่งต่อไปนี้
		``CREATE USER 'cloudmon'@'localhost' IDENTIFIED BY '123456789';``
		``GRANT SELECT ON *.* TO 'cloudmon'@'localhost' IDENTIFIED BY '123456789';``

Set time with crontab

	- sudo crontab -e
		- 0 0 1 * * sh /home/{User}/cloud-monitor-client/lynis-pdf.sh
		- * * * * * cd /home/{User}/cloud-monitor-client; sh cloud-monitor-clientscript.sh
		
How to Patch Script
	- cd cloud-monitor-client
	- git status
	- git pull

Enjoy