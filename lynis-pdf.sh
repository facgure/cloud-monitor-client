#!/bin/bash
BASEDIR=$(dirname "$0")
YM=$(date +%Y-%m)
YMD=$(date +%Y-%m-%d)
TMPDIR=$BASEDIR/temp
LYNISTMPDIR=$TMPDIR/lynis_security_report
TMPYMDIR=$LYNISTMPDIR/$YM
CONFFILE=$BASEDIR/config/cloud-monitor-clientconfig.conf
LYNIS_PDF="$TMPYMDIR"/"$YMD"_lynis.pdf
LYNIS_HTML="$TMPYMDIR"/"$YMD"_lynis.html
LYNIS_OUTPUT="$TMPYMDIR"/"$YMD"_lynis.o

if [ "$EUID" -ne 0 ]; then
    echo "Please run as root"
    exit 1
fi

if [ ! -f "$CONFFILE" ]; then
    echo "File config ($CONFFILE) not found!"
    exit 1
else
    . $CONFFILE
fi

#check service [lynis and ansi2html]
if ! [ -x "$(command -v lynis)" ]; then
    echo "Cann't used service 'lynis' or command not found!"
    exit 1
fi

if [ ! -f "$BASEDIR"/lib/ansi2html.sh ]; then
    echo "File ansi2html.sh not found!"
    exit 1
fi

if [ ! -d "$TMPDIR" ]; then
    mkdir $TMPDIR
    chmod 0777 $TMPDIR
fi

if [ ! -d "$LYNISTMPDIR" ]; then
    mkdir $LYNISTMPDIR
    chmod 0777 $LYNISTMPDIR
fi

if [ ! -d "$TMPYMDIR" ]; then
    mkdir $TMPYMDIR
    chmod 0777 $TMPYMDIR
fi

# audit start
printf "Lynis start progress ............ "
if [ -z "$(lynis --no-colors --check-all > "$LYNIS_OUTPUT")" ] ; then
    printf "\u001b[32mSuccess\u001b[0m\n"
else
    printf "\u001b[31mFailed\u001b[0m\n"
    exit 1
fi

printf "Convert to html ................. "
if [ -z "$(cat "$LYNIS_OUTPUT" | sh "$BASEDIR"/lib/ansi2html.sh > "$LYNIS_HTML")" ] ; then
    printf "\u001b[32mSuccess\u001b[0m\n"
else
    printf "\u001b[31mFailed\u001b[0m\n"
    exit 1
fi

printf "Convert to pdf .................. "
if [ -z "$(weasyprint "$LYNIS_HTML" "$LYNIS_PDF")" ] ; then
    printf "\u001b[32mSuccess\u001b[0m\n"
else
    printf "\u001b[31mFailed\u001b[0m\n"
    exit 1
fi

printf "Import lynis report to server --> "
if [ -f "$LYNIS_PDF" ]; then
    curl -s -X POST -F 'file=@"'$LYNIS_PDF'"' $HOSTNAME/UploadReport
else
    printf "\u001b[31mFailed\u001b[0m\n"
    exit 1
fi

exit 0