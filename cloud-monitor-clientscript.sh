#!/bin/bash

if ([ "$1" = "-h" ] || [ "$1" = "--help" ]);then
    echo "KEYWORD"
    echo "- ""write-data"
    echo "- ""cpu"
    echo "- ""memory"
    echo "- ""up-time"
    echo "- ""os-version"
    echo "- ""disk"
    echo "- ""container-docker"
    echo "- ""last-user"
    echo "- ""running-process"
    echo "- ""last-boot"
    echo "- ""success-login"
    echo "- ""sudo-command"
    echo "- ""fail-login"
    echo "- ""error-log"
    echo "- ""access-log"
    echo "- ""mysql-error"
    echo "- ""connect-database"
    echo "- ""database-rows"
    echo "- ""ithesis-server"  
exit 1
fi

BASEDIR=$(dirname "$0")
. $BASEDIR/config/cloud-monitor-clientconfig.conf
FILE_LIST=""

CHECK_CURL=$(curl -Is $HOSTNAME | head -n1 | sed -e 's/\r//g')
if [ "$CHECK_CURL" = "HTTP/1.1 200 OK" ]; then
  
if [ ! -d "$temp" ]; then
    mkdir -p "temp"
fi

if (([ "$CHECK_WRITEDATA_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "write-data" ]); then
    CHECK_WRITE_DATA()
    {
        echo '{"status":"Can create files"}'
    }
    CHECKDATA=$(CHECK_WRITE_DATA)
    TEMPFILE_WRITEDATA="temp/CHECK_WRITE_DATA.json"
    echo $CHECKDATA > $TEMPFILE_WRITEDATA
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_WRITEDATA\"'"
fi

if (([ "$CPU_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "cpu" ]); then
    CPU_USAGE()
    {
        top -p 678 -b -n3 |grep 'Cpu' | awk '{print "{\"Cpu-percent\":\""$2"\"}"}' | tail -1
    }
    CPU=$(CPU_USAGE)
    TEMPFILE_CPU="temp/CPU.json"
    echo $CPU > $TEMPFILE_CPU
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_CPU\"'"
fi

if (([ "$MAIN_MEM_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "memory" ]); then
    MAIN_MEMORY_AVERAGE()
    {
        free -m| jq -sR '[sub("\n$";"") | splits("\n") |sub("^ ";"memory-type")|[splits(" +")]] |.[0] as $header | .[1:] | [.[] | [. as $x | range($header | length) | {"key": $header[.], "value":$x[.]}] | from_entries]'
    }
    MAINMEMORY=$(MAIN_MEMORY_AVERAGE)
    TEMPFILE_MAINMEMORY="temp/MAIN_MEMORY.json"
    echo $MAINMEMORY > $TEMPFILE_MAINMEMORY
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_MAINMEMORY\"'"
fi

if (([ "$UP_TIME_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "up-time" ]); then
    UP_TIME()
    {   
        DAYS()
        {
            uptime | grep 'days'
        }
        CHKDAYS=$(DAYS)
        MIN()
        {
            uptime | grep 'min'
        }
        CHKMIN=$(MIN)
            if [ "$CHKDAYS" != "" ] && [ "$CHKMIN" != "" ]; then
                uptime | gawk -F '( |,|:)+' '
                BEGIN { ORS = ""; print " [ "}
                /Filesystem/ {next}
                { printf "%s{\"Days\": \""$6"\", \"Hours\": \""0"\" ,\"Minutes\":\""$8"\"}",
                separator,
                separator = ", "
                }
                END { print " ] " }'
            elif [ "$CHKDAYS" != "" ] && [ "$CHKMIN" = "" ]; then
                uptime | gawk -F '( |,|:)+' '
                BEGIN { ORS = ""; print " [ "}
                /Filesystem/ {next}
                { printf "%s{\"Days\": \""$6"\", \"Hours\": \""$8"\" ,\"Minutes\":\""$9"\"}",
                separator,
                separator = ", "
                }
                END { print " ] " }'
            elif [ "$CHKDAYS" = "" ] && [ "$CHKMIN" != "" ]; then
                uptime | gawk -F '( |,|:)+' '
                BEGIN { ORS = ""; print " [ "}
                /Filesystem/ {next}
                { printf "%s{\"Days\": \""0"\", \"Hours\": \""0"\" ,\"Minutes\":\""$6"\"}",
                separator,
                separator = ", "
                }
                END { print " ] " }'
            else
                uptime | gawk -F '( |,|:)+' '
                BEGIN { ORS = ""; print " [ "}
                /Filesystem/ {next}
                { printf "%s{\"Days\": \""0"\", \"Hours\": \""$6"\" ,\"Minutes\":\""$7"\"}",
                separator,
                separator = ", "
                }
                END { print " ] " }'
            fi
    }
    UPTIME=$(UP_TIME)
    TEMPFILE_UPTIME="temp/UPTIME.json"
    echo $UPTIME > $TEMPFILE_UPTIME
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_UPTIME\"'"
fi

if (([ "$OS_VERSION_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "os-version" ]); then
    OS_VERSION()
    {
        lsb_release -a | grep 'Description'| awk '{print "{ \"Description\":","\"",$2,$3,$4,$5,$6,"\"""}"}'
    }
    OS=$(OS_VERSION)
    TEMPFILE_OS="temp/OS.json"
    echo $OS > $TEMPFILE_OS
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_OS\"'"
fi

if (([ "$DISK_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "disk" ]); then
    DISK_USAGES()
    {
        df | sed -e "s/Mounted on/Mounted/g" | jq -sR '[sub("\n$";"") | splits("\n") | sub("^ +";"") | [splits(" +")]] | .[0] as $header | .[1:] | [.[] | [. as $x | range($header | length) | {"key": $header[.], "value":$x[.]}] | from_entries]'
    }
    DISK=$(DISK_USAGES)
    TEMPFILE_DISK="temp/DISK.json"
    echo $DISK > $TEMPFILE_DISK
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_DISK\"'"
fi

if (([ "$DOCKER_CONTAINER_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "container-docker" ]); then
    CONTAINER_IN_DOCKER()
    {
        printf "["
		chkcontainer=1
		docker ps --format='{{json .}}' | \
 		while IFS= read -r  line
		do
			if [ "$chkcontainer" = 1 ];
                        then
                            chkcontainer=0
                        else
                            printf ","
                        fi

			echo $line
		done
	    printf "]"
    }
    DOCKER=$(CONTAINER_IN_DOCKER)
    TEMPFILE_DOCKER="temp/DOCKER.json"
    echo $DOCKER > $TEMPFILE_DOCKER
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_DOCKER\"'"
fi

if (([ "$LAST_USER_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "last-user" ]); then
    LAST_USER_ACCESS()
    {
        last -w| grep 'pts'|head -n 10| gawk '
            BEGIN { ORS = ""; print " [ "}
            /Filesystem/ {next}
            { printf "%s{\"Username\": \""$1"\", \"LastAccess\": \""$4" "$5" "$6" "$7"\"}",
            separator,
            separator = ", "
            }
            END { print " ] " }'
    }
    USERACCESS=$(LAST_USER_ACCESS)
    TEMPFILE_USERACCESS="temp/USER_ACCESS.json"
    echo $USERACCESS > $TEMPFILE_USERACCESS
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_USERACCESS\"'"
fi

if (([ "$RUNNING_PROCESS_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "running-process" ]); then
    RUNNING_PROCESS()
    {
        ps aux --sort -%cpu | head -n11 | jq -sR '[sub("\n$";"") | splits("\n") | sub("^ +";"") | [splits(" +")]] | .[0] as $header | .[1:] | [.[] | [. as $x | range($header | length) | {"key": $header[.], "value":$x[.]}] | from_entries]'
    }
    RUNNINGPROCESS=$(RUNNING_PROCESS)
    TEMPFILE_RUNNINGPROCESS="temp/RUNNING_PROCESS.json"
    echo $RUNNINGPROCESS > $TEMPFILE_RUNNINGPROCESS
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_RUNNINGPROCESS\"'"
fi

if (([ "$LAST_BOOT_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "last-boot" ]); then
    LAST_BOOT()
    {
       last -F reboot | grep 'reboot'|head -n 1| gawk 'BEGIN { ORS = ""; print " [ "}/Filesystem/ {next}{ printf "%s{\"Lastreboot\": \""$7" "$5" "$6" "$9" "$8"\"}",separator,separator = ", " }END { print " ] " }'
    }
    LASTBOOT=$(LAST_BOOT)
    TEMPFILE_LASTBOOT="temp/LASTBOOT.json"
    echo $LASTBOOT > $TEMPFILE_LASTBOOT
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_LASTBOOT\"'"
fi

if (([ "$CHECK_SUCCESS_LOGIN_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "success-login" ]); then
    CHECK_ACCESS_TO_BACKEND_SUCCESS_LOGIN()
    {
             tail -n300 $LOG_SECURE_CONFIG |grep 'Accepted'|tail -n10 |gawk '
                BEGIN { ORS = ""; print " [ "}
                /Filesystem/ {next}
                { printf "%s{\"Date\": \""$1" "$2" "$3"\", \"sshd-number\": \""$5" \", \"status\": \" "$6" "$7" "$8" "$9" "$10" "$11" "$12" "$13" \"}",
                separator,
                separator = ", "
                }
                END { print " ] " }'      
    }
    BACKEND_SUCCESSLOGIN=$(CHECK_ACCESS_TO_BACKEND_SUCCESS_LOGIN)
    TEMPFILE_BACKEND_SUCCESSLOGIN="temp/BACKEND_SUCCESSLOGIN.json"
    echo $BACKEND_SUCCESSLOGIN > $TEMPFILE_BACKEND_SUCCESSLOGIN
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_BACKEND_SUCCESSLOGIN\"'"
fi

if (([ "$CHECK_SUDOCOMMAND_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "sudo-command" ]); then
    CHECK_ACCESS_TO_BACKEND_SUDO_COMMAND()
    {
             tail -n300 $LOG_SECURE_CONFIG |grep 'COMMAND'| tail -n10 |sed 's/\\/\\\\/g'|sed 's/\"/\\"/g'| gawk '
                BEGIN { ORS = ""; print " [ "}
                /Filesystem/ {next}
                { printf "%s{\"Date\": \""$1" "$2" "$3"\", \"User-sudo\": \""$6" \", \"Command\": \""$14" "$15" "$16" "$17" "$18" "$19" "$20"\"}",
                separator,
                separator = ", "
                }
                END { print " ] " }'
    }
    BACKEND_SUDOCOMMAND=$(CHECK_ACCESS_TO_BACKEND_SUDO_COMMAND)
    TEMPFILE_BACKEND_SUDOCOMMAND="temp/BACKEND_SUDOCOMMAND.json"
    echo $BACKEND_SUDOCOMMAND > $TEMPFILE_BACKEND_SUDOCOMMAND
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_BACKEND_SUDOCOMMAND\"'"
fi

if (([ "$CHECK_LOGIN_FAIL_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "fail-login" ]); then
    CHECK_ACCESS_TO_BACKEND_FAIL_LOGIN()
    {       
             tail -n300 $LOG_SECURE_CONFIG |grep 'Invalid\|Disconnected\|error'| tail -n10 | gawk '
                BEGIN { ORS = ""; print " [ "}
                /Filesystem/ {next}
                { printf "%s{\"Date\": \""$1" "$2" "$3"\", \"sshd-number\": \""$5" \", \"status\": \" "$6" "$7" "$8" "$9" "$10" "$11" "$12" "$13" "$14" "$15" "$16" "$17" \"}",
                separator,
                separator = ", "
                }
                END { print " ] " }'  
    }
    BACKEND_FAILLOGIN=$(CHECK_ACCESS_TO_BACKEND_FAIL_LOGIN)
    TEMPFILE_BACKEND_FAILLOGIN="temp/BACKEND_FAILLOGIN.json"
    echo $BACKEND_FAILLOGIN > $TEMPFILE_BACKEND_FAILLOGIN
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_BACKEND_FAILLOGIN\"'"
fi

if (([ "$CHECK_ERROR_LOG_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "error-log" ]); then
    CHECK_ERROR_LOG()
    {
            tail -n 300 $ERROR_LOG_CONFIG | grep 'core:error' | tail -n20 |sed 's/\"/\\"/g'| awk '
                BEGIN { ORS = ""; print " [ "}
                /Filesystem/ {next}
                { printf "%s{\"Date\": \""$1" "$2" "$3" "$5"\",\"Time\":\""$4"\",\"Error-log\":\""$6" "$7" "$8" "$9" "$10" "$11" "$12" "$13" "$14" "$15"\"}",
                separator,
                separator = ", "
                }
                END { print " ] " }'
    }
    ERRORLOG=$(CHECK_ERROR_LOG)
    TEMPFILE_ERROR_LOG="temp/CHECK_ERROR_LOG.json"
    echo $ERRORLOG > $TEMPFILE_ERROR_LOG
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_ERROR_LOG\"'"
fi

if (([ "$CHECK_ACCESS_LOG_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "access-log" ]); then
    CHECK_ACCESS_LOG()
    {
            tail -n 300 $ACCESS_LOG_CONFIG | grep 'HTTP/1.1" 201\|HTTP/1.1" 204\|HTTP/1.1" 304\|HTTP/1.1" 400\|HTTP/1.1" 401\|HTTP/1.1" 403\|HTTP/1.1" 404\|HTTP/1.1" 409\|HTTP/1.1" 500' \
            | tail -n20 |sed 's/\"/\\"/g'| awk 'BEGIN { ORS = ""; print " [ "}
                /Filesystem/ {next}
                { printf "%s{\"Ip-address\": \""$1"\", \"Date\": \""$4"\" ,\"Error-log\":\""$6" "$7" "$8" "$9" "$10" "$11" "$12" "$13" "$14" "$15" "$16" "$17" "$18" "$19" "$20"\"}",
                separator,
                separator = ", "
                }
                END { print " ] " }'
    }
    ACCESSLOG=$(CHECK_ACCESS_LOG)
    TEMPFILE_ACCESS_LOG="temp/CHECK_ACCESS_LOG.json"
    echo $ACCESSLOG > $TEMPFILE_ACCESS_LOG
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_ACCESS_LOG\"'"
fi

if (([ "$MYSQL_ERROR_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "mysql-error" ]); then
    CHECK_MYSQL_ERROR()
    {
            docker exec $DB_CONTAINERNAME sh -c "tail -n300 $MYSQL_ERROR_LOG_CONFIG"| grep 'Note\|Warning\|ERROR' | tail -n20 |sed -e "s/\r//g" |awk '
                BEGIN { ORS = ""; print " [ "}
                /Filesystem/ {next}
                { printf "%s{\"Log-number\": \""$1"\", \"Time\": \""$2"\" ,\"Error-log\":\""$3" "$4" "$5" "$6" "$7" "$8" "$9" "$10" "$11"\"}",
                separator,
                separator = ", "
                }
                END { print " ] " }'
    }
    MYSQLERROR=$(CHECK_MYSQL_ERROR)
    TEMPFILE_MYSQL_ERROR="temp/CHECK_MYSQL_ERROR.json"
    echo $MYSQLERROR > $TEMPFILE_MYSQL_ERROR
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_MYSQL_ERROR\"'"
fi

if (([ "$CONNECT_DB_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "connect-database" ]); then
    CONNECTION_DATABASE()
    {
        if [ "$DB_ON_DOCKER" = "yes" ]; then
            CON=$(docker exec $DB_CONTAINERNAME mysql -u$USERNAME_DB -p$PASSWORD_DB -e status)
            if [ "$CON" != "" ]; then
                docker exec $DB_CONTAINERNAME mysql -u$USERNAME_DB -p$PASSWORD_DB -e status |grep 'Uptime' |sed -e "s/\r//g" | awk '{print"{\"Connect\":true,\"Uptime\": \""$2,$3,$4,$5,$6,$7"\","}' \
                && docker exec $DB_CONTAINERNAME mysql -u$USERNAME_DB -p$PASSWORD_DB -e status |grep 'Threads' |sed -e "s/\r//g" |awk '{print "\"Threads\":\""$2"\",\"Questions\":\""$4"\",\"Slow_queries\":\""$7"\",\"Opens\":\""$9"\",\"Flush_tables\":\""$12"\",\"Open_tables\":\""$15"\",\"Queries_per_second_avg\":\" "$20"\"}"}'
            else
               echo '{"Connect":false,"Uptime":"","Threads":"","Questions":"","Slow_queries":"","Opens":"","Flush_tables":"","Open_tables":"","Queries_per_second_avg":""}'
            fi
        elif [ "$DB_ON_DOCKER" = "no" ]; then
            CON=$(mysql -h$HOSTNAME_DATABASET -u$USERNAME_DB -p$PASSWORD_DB -e status)
            if [ "$CON" != "" ]; then
                mysql -h$HOSTNAME_DATABASET -u$USERNAME_DB -p$PASSWORD_DB -e status |grep 'Uptime' |sed -e "s/\r//g" | awk '{print"{\"Connect\":true,\"Uptime\": \""$2,$3,$4,$5,$6,$7"\","}' \
                && mysql -h$HOSTNAME_DATABASET -u$USERNAME_DB -p$PASSWORD_DB -e status |grep 'Threads' |sed -e "s/\r//g" |awk '{print "\"Threads\":\""$2"\",\"Questions\":\""$4"\",\"Slow_queries\":\""$7"\",\"Opens\":\""$9"\",\"Flush_tables\":\""$12"\",\"Open_tables\":\""$15"\",\"Queries_per_second_avg\":\" "$20"\"}"}'
            else
               echo '{"Connect":false,"Uptime":"","Threads":"","Questions":"","Slow_queries":"","Opens":"","Flush_tables":"","Open_tables":"","Queries_per_second_avg":""}'
            fi
        fi
    }
    CONNECTION_DB=$(CONNECTION_DATABASE)
    TEMPFILE_CONNECTION_DATABASE="temp/CONNECTION_DATABASE.json"
    echo $CONNECTION_DB > $TEMPFILE_CONNECTION_DATABASE
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_CONNECTION_DATABASE\"'"
fi

if (([ "$DATABASE_ROW_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "database-rows" ]); then
    DATABASE_ROW()
    {
            printf "["
            chkLoopDB=1
            docker exec $DB_CONTAINERNAME sh -c "mysql -u$USERNAME_DB -p$PASSWORD_DB -s -N -e 'SHOW DATABASES;'" | sed -e 's/\r//g' |  \
            while IFS= read -r  databaseName
            do
                if [ $databaseName != information_schema ] && [ $databaseName != mysql ] && [ $databaseName != performance_schema ];
                then
                    if [ "$chkLoopDB" = 1 ];
                    then
                        chkLoopDB=0
                    else
                        printf ","
                    fi	

                    chkLoopTB=1
                    printf "{"
                    printf \"$databaseName\":
                    printf "{"

                    docker exec  $DB_CONTAINERNAME sh -c "mysql -u$USERNAME_DB -p$PASSWORD_DB -s -N -e 'USE \```$databaseName``\`;SHOW TABLES;' " | sed -e 's/\r//g' | \
                        while IFS= read -r  tableName
                        do
                            if [ "$chkLoopTB" = 1 ];
                            then
                                chkLoopTB=0
                            else
                                printf ","
                            fi

                            printf \"$tableName\":
                            printf $(docker exec  $DB_CONTAINERNAME sh -c "mysql -u$USERNAME_DB -p$PASSWORD_DB -s -N -e 'SELECT COUNT(1) FROM \```$databaseName``\`.\```$tableName``\`;'")
                        done 
                    printf "}"
                    printf "}"

                fi
            done
            printf "]"
    }
    DATABASEROW=$(DATABASE_ROW)
    TEMPFILE_DATABASE_ROW="temp/DATABASE_ROW.json"
    echo $DATABASEROW > $TEMPFILE_DATABASE_ROW
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_DATABASE_ROW\"'"
fi

if (([ "$ITHESIS_SERVER_CONNECTION_LOG_CONFIG" = on ] && [ "$1" = "" ]) || [ "$1" = "ithesis-server" ]); then
    ITHESIS_SERVER_CONNECTION()
    {
        LOG=$(ls $ITHESIS_LOG/*/system_status/*/*.log | tail -n1)
        cat $LOG | tail -n1 | awk ' 
                BEGIN { ORS = ""; print " [ "}
                /Filesystem/ {next}
                { printf " {\""$4"\":"$5" \""$6"\":"$7" \""$8"\":"$9" \""$10"\":"$11" \""$12"\":"$13" \""$14"\":"$15" }",
                separator,
                separator = ", "
                }
                END { print " ] " }' | sed 's/\;//g'
    }
    ITHESIS_SERVER_LOG=$(ITHESIS_SERVER_CONNECTION)
    TEMPFILE_ITHESIS_SERVER="temp/ITHESIS_SERVER_CONNECTION.json"
    echo $ITHESIS_SERVER_LOG > $TEMPFILE_ITHESIS_SERVER
    FILE_LIST=$FILE_LIST" -F 'file=@\"$TEMPFILE_ITHESIS_SERVER\"'"
fi

#sent
Send_MainServer()
{
    curl -X POST -F client-key=$CLIENT_KEY \ $FILE_LIST $HOSTNAME/UploadToMainServer
}

#Send To Main Server
sendResult=$(Send_MainServer)

else
    echo "Failed Connection"
    exit 1
fi